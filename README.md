# Mx. Shift USB-2G

USB-2G is a USB adapter designed for MoTeC 2-Group ECUs.

When I announced [USB M8](https://github.com/mx-shift/usb_m8) being available, I
had a few inquries about 2-Group support. 2-Group also uses a PCI cable but
needs a baud rate generator. That's a fairly straightforward enhancement to USB
M8 so I created USB 2G. I took the opportunity to make some improvements to the
original USB M8 design such as adding electrical isolation.

## Where can I get one?

Tindie, eventually. I need to finish the board layout and build a few to test.
If you have a 2-Group ECU and are willing to help try out a cable, please get in
touch.  The schematics and board layout files will be available in this
repository once the design is tested.

If you want to follow along with the development progress, look at the other
branches in the repository.


## Wait. So, I can build my own?

Yup. While I offer finished cables, anyone is free to use the design files here
to produce their own PCBs, full cables, or even improved designs. The only
requirement is attribution per [Creative Commons Attribution 4.0 International
License](https://creativecommons.org/licenses/by/4.0/).

## If anyone can make them, why are you selling them?

Not everyone has the equipment or desire to assemble their own.  For those individuals, I offer fully assembled and tested cables.
